import os
import shutil
from transliterate import translit

MAX_NAME_LENGTH = 250


def create_filename(filename, length):
    name, extension = os.path.splitext(filename)
    if len(name) > length:
        name = name[:length]
    name = translit(name, 'ru').replace("_", " ")
    return f"{name}{extension}"


def copy_with_transliteration(source_dir, destination_dir, max_length):
    for root, _, files in os.walk(source_dir):
        relative_path = os.path.relpath(root, source_dir)
        destination_subdir = os.path.join(destination_dir, relative_path)

        if not os.path.exists(destination_subdir):
            os.makedirs(destination_subdir)

        for file in files:
            source_path = os.path.join(root, file)
            destination_path = os.path.join(destination_subdir, create_filename(file, max_length))
            shutil.copy(source_path, destination_path)
            print(f"Скопирован файл {file} в {destination_path}")


if __name__ == "__main__":
    try:
        source_directory = input("Введите путь к исходной директории: ")
        destination_directory = input("Введите путь к целевой директории: ")
        max_length = int(input(f"Введите максимальную длину имени файла ({MAX_NAME_LENGTH}): ") or str(MAX_NAME_LENGTH))

        copy_with_transliteration(source_directory, destination_directory, max_length)
        print("Нажмите любую клавишу для выхода")
        input()
    except Exception as e:
        print(str(e))
        input()
